const contentful = require('contentful')

const pkg = require('./package')

const apis = contentful.createClient({
  // This is the space ID. A space is like a project folder in Contentful terms
  space: 'r5qfjn2gyt3z',
  // This is the access token for this space. Normally you get both ID and the token in the Contentful web app
  accessToken:
    '14f332236469b52d6bf21ca76b01813fdd33fb53cc8c3c9719992a4fdec1fa3a'
})

module.exports = {
  mode: 'universal',
  modern: true,

  /*
   ** Headers of the page
   */
  head: {
    title: 'Blog',
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: pkg.description
      },
      {
        hid: 'version',
        name: 'version',
        content: pkg.version
      },
      {
        hid: 'author',
        name: 'author',
        content: pkg.author
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff'
  },

  /*
   ** Global CSS
   */
  css: ['~/assets/styles/font.scss', '~/assets/styles/bulma.scss'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/axios.js'],

  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/google-analytics',
    'nuxt-fontawesome',
    '@nuxtjs/markdownit'
  ],

  'google-analytics': {
    id: 'UA-139626672-1'
  },

  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  // [optional] markdownit options
  // See https://github.com/markdown-it/markdown-it
  markdownit: {
    injected: true,
    preset: 'default',
    linkify: true,
    breaks: true
  },

  fontawesome: {
    component: 'fa',
    imports: [
      //import whole set
      {
        set: '@fortawesome/free-solid-svg-icons',
        icons: ['fas']
      },
      {
        set: '@fortawesome/free-regular-svg-icons',
        icons: ['far']
      },
      {
        set: '@fortawesome/free-brands-svg-icons',
        icons: ['fab']
      }
    ]
  },

  generate: {
    fallback: '404.html',
    routes: function() {
      return apis
        .getEntries({
          content_type: 'blogPost'
        })
        .then(result => {
          const response = result.items.reduce((previous, article) => {
            if (!previous) previous = []

            if (
              !previous.includes(
                '/authors/' + article.fields.author.fields.facebook
              )
            )
              previous.push({
                route: '/authors/' + article.fields.author.fields.facebook,
                payload: article.fields.author
              })

            previous.push({
              route: '/posts/' + article.fields.slug,
              payload: article
            })

            return previous
          }, [])
          return response
        })
    }
  },

  /*
   ** Build configuration
   */
  build: {
    extractCSS: true,
    publicPath: '/lib/',
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, { isDev, isClient, loaders }) {
      if (isDev) loaders.cssModules.localIdentName = '[name]_[local]'
      else
        loaders.cssModules.localIdentName =
          'kcnt__[name]_[contenthash:base64:18]'

      // Run ESLint on save
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
